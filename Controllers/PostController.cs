﻿using Blog.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Controllers
{
    [Authorize]
    public class PostController : Controller
    {

        /// <summary>
        /// Shows all posts in database
        /// </summary>
        /// <returns></returns>
        // GET: Post
        public ActionResult Index()
        {
            List<Post> posts = new List<Post>();
            using (ApplicationDbContext context = ApplicationDbContext.Create())
            {
                posts = context.Posts
                    .Include("User")
                    .ToList();

                return View(posts);
            }
        }

        /// <summary>
        /// Create or edit an existing post.
        /// If id is null: create new post
        /// </summary>
        /// <param name="id">id of the post to edit</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateEdit(int? id)
        {
            using (ApplicationDbContext context = ApplicationDbContext.Create())
            {
                Post model = new Post();

                if (context.Posts.Find(id) != null)
                {
                    model = context.Posts.Find(id);
                }

                return View("CreateEdit", model);
            }
        }

        /// <summary>
        /// If not existing, directory "/Pictures" is created and the uploadPicture-image is uploaded in that directory. The post also get saved in the database.
        /// </summary>
        /// <param name="post"></param>
        /// <param name="uploadPicture"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateEdit(Post post, HttpPostedFileBase uploadPicture)
        {
            if (post != null)
            {
                string directory = Server.MapPath("/Pictures");
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                using (ApplicationDbContext context = ApplicationDbContext.Create())
                {
                    if (uploadPicture != null && context.Posts.Find(post.Id) == null)
                    {
                        string fileExtension = Path.GetExtension(uploadPicture.FileName);
                        post.User = context.Users.Find(User.Identity.GetUserId());
                        post.ImageExtension = fileExtension;
                        post.Timestamp = DateTime.Now;
                        context.Posts.Add(post);
                        context.SaveChanges();

                        string id = post.Id.ToString();
                        string filename = id + fileExtension;
                        string path = Path.Combine(directory, filename);
                        uploadPicture.SaveAs(path);
                        context.SaveChanges();
                    }
                    else
                    {
                        Post dbPost = context.Posts.Find(post.Id);
                        if (dbPost != null)
                        {
                            dbPost.Caption = post.Caption;
                            context.Entry(dbPost);
                            context.SaveChanges();
                        }
                        else
                        {
                            return View(post);
                        }
                    }

                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Create");
        }

        /// <summary>
        /// If the current user is the user from the wanting to be deleted post; delete post.
        /// </summary>
        /// <param name="id"></param>
        public ActionResult Delete(int? id)
        {
            using (ApplicationDbContext context = ApplicationDbContext.Create())
            {
                if (id != null)
                {
                    Post post = context.Posts.Find(id);
                    if (post != null)
                    {
                        if (User.Identity.GetUserId() == post.UserId)
                        {
                            string path = Server.MapPath("/Pictures/") + post.Id + post.ImageExtension;
                            System.IO.File.Delete(path);
                            context.Posts.Remove(post);
                            context.SaveChanges();
                        }
                    }
                }
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Sortiert die Posts nach Datum
        /// </summary>
        /// <param name="filter">Sortier-Art</param>
        /// <returns>Sortierte Liste</returns>
        [HttpPost]
        public ActionResult Filter(string filter)
        {
            List<Post> posts = new List<Post>();
            if (filter == "Datum")
            {
                using (ApplicationDbContext context = ApplicationDbContext.Create())
                {
                    posts = context.Posts
                    .Include("User")
                    .ToList();

                    posts = posts.OrderByDescending(p => p.Timestamp).ToList();
                }
            }
            return View("Index", posts);
        }

        /// <summary>
        /// Zeigt alle Posts eines spezifischen Users an
        /// </summary>
        /// <param name="id">Id vom User</param>
        /// <returns>Alle Posts des Users</returns>
        public ActionResult MeinePosts(string id)
        {
            List<Post> posts = new List<Post>();
            using (ApplicationDbContext context = ApplicationDbContext.Create())
            {
                posts = context.Posts
                    .Include("User")
                    .Where(p => p.UserId == id)
                    .ToList();
            }
            return View("Index", posts);
        }
    }
}