﻿using Blog.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Blog.BL
{
    [Authorize]
    public class UserController : Controller
    {
        /// <summary>
        /// GET: alle User aus der Datenbank
        /// </summary>
        /// <returns>Eine Liste aller User</returns>
        [HttpGet]
        public ActionResult Index()
        {
            List<ApplicationUser> users = new List<ApplicationUser>();
            using (ApplicationDbContext context = ApplicationDbContext.Create())
            {
                users = context.Users.ToList();

                return View("", users);
            }
        }

        /// <summary>
        /// POST: sucht nach Usern, welche den Suchparameter im Namen enthalten.
        /// </summary>
        /// <param name="search">Text, welcher im Namen des Users vokommen muss</param>
        /// <returns>Alle User, welche den Suchparameter im Username enthalten</returns>
        [HttpPost]
        public ActionResult Search(string search)
        {
            List<ApplicationUser> users = new List<ApplicationUser>();
            using (ApplicationDbContext context = ApplicationDbContext.Create())
            {
                users = context.Users.Include(u => u.Posts).ToList();
                users = users.Where(u => u.UserName.Contains(search)).ToList();
                return View("Index", users);
            }
        }

        /// <summary>
        /// Öffnet das Profil eines spezifischen User und zeigt alle seine Posts an.
        /// </summary>
        /// <param name="id">Username des gesuchten Users</param>
        /// <returns>Das Profil eines spezifischen Users</returns>
        public ActionResult Detailed(string id = "")
        {
            using (ApplicationDbContext context = ApplicationDbContext.Create())
            {
                ApplicationUser user = context.Users.Include(u => u.Posts).Where(u => u.UserName == id).FirstOrDefault();
                return View(user);
            }
        }
    }
}