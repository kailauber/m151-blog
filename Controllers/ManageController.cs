﻿using Blog.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Blog.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get => _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            private set => _signInManager = value;
        }

        public ApplicationUserManager UserManager
        {
            get => _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set => _userManager = value;
        }

        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                                : message == ManageMessageId.SetPasswordSuccess ? "Your username has been set."

                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            string userId = User.Identity.GetUserId();
            List<Post> posts = new List<Post>();
            using (ApplicationDbContext context = ApplicationDbContext.Create())
            {
                string username = User.Identity.GetUserName();
                posts = context.Users.Where(u => u.UserName == username).Include(u => u.Posts).FirstOrDefault().Posts.ToList();
            }
            IndexViewModel model = new IndexViewModel
            {
                Id = User.Identity.GetUserId(),
                Username = User.Identity.GetUserName(),
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
                Logins = await UserManager.GetLoginsAsync(userId),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(userId),
                Posts = posts
            };
            return View(model);
        }

        /// <summary>
        /// Gets Only Users Posts from Database Context and returns in model 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult Detailed(DetailedViewModel model)
        {
            List<Post> posts = new List<Post>();
            using (ApplicationDbContext context = ApplicationDbContext.Create())
            {
                posts = context.Posts.Where(u => u.UserId == model.Id).ToList();
            }
            return View(posts);
        }

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                UserProfileSessionData profileData = new UserProfileSessionData
                {
                    Username = model.OldPassword,
                    Password = model.NewPassword
                };

                Session["UserProfile"] = profileData;
                return View(model);
            }
            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }

            return View(model);
        }

        /// <summary>
        /// Logs the user off and returns him to Login page
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Manage/ChangeUsername
        public ActionResult ChangeUsername()
        {
            return View();
        }

        //
        // POST: /Manage/ChangeUsername
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeUsername(ChangeUsernameViewModel model)
        {
            if (model.NewUsername == model.ConfirmUsername)
            {
                using (ApplicationDbContext context = ApplicationDbContext.Create())
                {
                    ApplicationUser u = context.Users.Where(x => x.UserName == model.OldUsername).SingleOrDefault();
                    u.UserName = model.NewUsername;
                    context.SaveChanges();
                    return LogOff();
                }
            }
            else
            {
                using (ApplicationDbContext context = ApplicationDbContext.Create())
                {
                    ApplicationUser u = context.Users.Where(x => x.UserName == model.OldUsername).SingleOrDefault();
                    u.UserName = model.OldUsername;
                    context.SaveChanges();
                    return LogOff();
                }
            }


        }


        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        private bool HasPassword()
        {
            ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangeUsernameSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion
    }
}