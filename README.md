# M151 Blog

M151 Blog ist eine Webapplikation, bei welcher Sie sich anmelden & Bilder hochladen können.

## Allgemeine Projektangaben

Die Applikation verfügt über Registrierungs- und Login-Funktionen. Die URLs der aufgeladenen Posts werden mitsamt Id in einer SQL Server Express Datenbank gespeichert. Ein Benutzer kann auf seiner Profilseite seine Angaben wie Benutzername oder Passwort ändern, und seine eigenen Posts anschauen. Die Posts können von dort auch gelöscht und bearbeitet werden. Auf der Blogseite "Posts" sieht man seine eigenen, sowie Posts der anderen Benutzer. Diese Posts können nach Anzahl Upvotes oder Datum sortiert werden. Ausserdem kann man auf dieser Seite auch Posts löschen und Liken. Die Applikation verfügt ausserdem über eine Seite mit Zusammenfassung aller Benutzer. 



## Technische Rahmenbedingungen

Folgendes wurde verwendet:

- Visual Studio 2019 (IDE)
- SQL Server Express
- Gitlab
- Git

## Installation

Die Applikation verwendet eine SQL Server Express Datenbank, um die Daten zu speichern. Diese wird bei der ersten Registrieung eines Benutzers erstellt. 
