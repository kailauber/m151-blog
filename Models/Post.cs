﻿using System;

namespace Blog.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string ImageExtension { get; set; }
        public string Caption { get; set; }
        public DateTime Timestamp { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}