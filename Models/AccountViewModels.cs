﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Blog.Models
{

    [Serializable]
    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Benutzername")]
        public string Username { get; set; }
    }

    [Serializable]
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Benutzername")]

        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Passwort")]
        public string Password { get; set; }

        [Display(Name = "Eingeloggt bleiben?")]
        public bool RememberMe { get; set; }
    }
    [Serializable]
    public class RegisterViewModel
    {
        [Required]

        [Display(Name = "Benutzername")]
        public string Username { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Das {0} muss mindestens {2} Zeichen lang sein.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Passwort")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Passwort bestätigen")]
        [Compare("Password", ErrorMessage = "Die Passwörter stimmen nicht überein.")]
        public string ConfirmPassword { get; set; }
    }
    [Serializable]
    public class ResetPasswordViewModel
    {
        [Required]

        [Display(Name = "Benutzername")]
        public string Username { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Das {0} muss mindestens {2} Zeichen lang sein.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Passwort")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Passwort bestätigen")]
        [Compare("Password", ErrorMessage = "Die Passwörter stimmen nicht überein.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
    [Serializable]
    public class ForgotPasswordViewModel
    {
        [Required]

        [Display(Name = "Benutzername")]
        public string Username { get; set; }
    }
}
