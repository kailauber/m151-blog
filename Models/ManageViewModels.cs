﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Blog.Models
{
    [Serializable]
    public class UserProfileSessionData
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public List<Post> Posts { get; set; }
    }

    public class IndexViewModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
        public List<Post> Posts { get; set; }
    }
    [Serializable]
    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangeUsernameViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Aktueller Benutzername")]
        public string OldUsername { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Der {0} muss mindestens {2} Zeichen lang sein.", MinimumLength = 1)]
        [DataType(DataType.Text)]
        [Display(Name = "Neuer Benutzername")]
        public string NewUsername { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Neuen Benutzernamen bestätigen")]
        [Compare("NewUsername", ErrorMessage = "Die Benutzernamen stimmen nicht überein.")]
        public string ConfirmUsername { get; set; }
    }

    public class DetailedViewModel
    {
        public string Id { get; set; }
    }
}